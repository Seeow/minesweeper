﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Minesweeper
{
    public class MenuItem
    {
        public string Title { get; private set; }
        public Rectangle Bounds { get; set; }
        public bool Active { get; set; }
        public GameState.State ScreenTarget { get; private set; }
        public Dictionary<string, string> Options { get; set; }
        public int GameGridWidth { get; private set; }
        public int GameGridHeight { get; private set; }
        public int GameMines { get; private set; }

        public MenuItem(string title, GameState.State screenTarget, bool active = false)
        {
            Title = title;
            Active = active;
            ScreenTarget = screenTarget;
        }

        public MenuItem(string title, GameState.State screenTarget, int width, int height, int mines, bool active = false) : this(title, screenTarget, active)
        {
            GameGridWidth = width;
            GameGridHeight = height;
            GameMines = mines;
        }
    }
}
