﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Minesweeper
{
    public class Cell
    {
        public List<Texture2D> Textures { get; set; }
        public int Texture { get; set; }
        public Vector2 Position { get; set; }
        public int Size { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int MarginRight { get; set; }
        public int MarginBottom { get; set; }
        public bool Mine { get; set; }
        public SpriteFont Font { get; private set; }
        public int NearbyMines { get; set; }
        public int GridLine { get; set; }
        public int GridColumn { get; set; }

        public Cell(List<Texture2D> textures, Vector2 position, int size, int marginRight, int marginBottom, int gridLine, int gridColumn)
        {
            Textures = textures;
            Texture = 0;
            Position = position;
            Size = size;
            MarginRight = marginRight;
            MarginBottom = marginBottom;
            Width = Size - MarginRight;
            Height = Size - MarginBottom;
            Mine = false;
            NearbyMines = 0;
            GridLine = gridLine;
            GridColumn = gridColumn;

            LoadContent();
        }

        public void LoadContent()
        {
            Font = Game1.Instance.Content.Load<SpriteFont>("fonts/game");
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Textures[Texture], new Rectangle((int)Position.X, (int)Position.Y, Width, Height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);

            if (Texture == 1 && NearbyMines > 0) {
                string str = NearbyMines.ToString();
                int x = (int)Position.X + (Width / 2) - ((int)Font.MeasureString(str).X / 2);
                int y = (int)Position.Y + (Height / 2) - ((int)Font.MeasureString(str).Y / 2);
                Color color;
                switch (NearbyMines) {
                    case 2:
                        color = Color.Green;
                        break;
                    case 3:
                        color = Color.Orange;
                        break;
                    case 4:
                        color = Color.OrangeRed;
                        break;
                    case 5:
                        color = Color.DarkRed;
                        break;
                    case 6:
                        color = Color.Purple;
                        break;
                    case 7:
                        color = Color.DarkViolet;
                        break;
                    case 8:
                        color = Color.Black;
                        break;
                    case 1:
                    default:
                        color = Color.Blue;
                        break;
                }
                spriteBatch.DrawString(Font, str, new Vector2(x, y), color, 0, Vector2.Zero, 1, SpriteEffects.None, 0.9f);
            }
        }
    }
}
