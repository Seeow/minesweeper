﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace Minesweeper
{
    public class GameScreen : Screen
    {
        private static GameScreen instance;

        public static GameScreen Instance {
            get {
                if (instance == null) {
                    instance = new GameScreen();
                }
                return instance;
            }
            set {
                instance = value;
            }
        }
        public Grid Grid { get; private set; }
        public SpriteFont Font { get; private set; }
        public int Mines { get; set; }
        public int Flags { get; private set; }
        public int GridWidth { get; set; }
        public int GridHeight { get; set; }
        public SoundEffect SoundFlag { get; private set; }
        public SoundEffect SoundRemoveFlag { get; private set; }
        public SoundEffect SoundExplosion { get; private set; }
        public TimeSpan Time { get; set; }

        public GameScreen()
        {
            Mines = 10;
            GridWidth = 9;
            GridHeight = 9;
            Flags = 0;

            Grid = new Grid(GridWidth, GridHeight, Mines);

            LoadContent();
        }

        public GameScreen(int mines, int gridWidth, int gridHeight)
        {
            Mines = mines;
            GridWidth = gridWidth;
            GridHeight = gridHeight;
            Flags = 0;

            Grid = new Grid(GridWidth, GridHeight, Mines);

            LoadContent();
        }

        public override void LoadContent()
        {
            Font = Content.Load<SpriteFont>("fonts/game");
            Song = Content.Load<Song>("musics/A Life");
            SoundFlag = Content.Load<SoundEffect>("sounds/flag");
            SoundRemoveFlag = Content.Load<SoundEffect>("sounds/remove_flag");
            SoundExplosion = Content.Load<SoundEffect>("sounds/explosion");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Time = Time.Add(gameTime.ElapsedGameTime);

            if (GameState.KeyboardState.KeyPressedOnce(Keys.Escape)) {
                Change(GameState.State.PauseMenu);
            }

            int countInactiveCells = 0;
            foreach (Cell[] line in Grid.Cells) {
                foreach (Cell cell in line) {
                    if (cell.Texture != 1) {
                        countInactiveCells++;
                    }
                }
            }

            if (countInactiveCells <= Mines) {
                Change(GameState.State.GameEnd);
            }

            if (GameState.MouseState.RightButton == ButtonState.Pressed && GameState.LastMouseState.RightButton == ButtonState.Released) {
                Cell cellClicked = Grid.ClickOnCell();
                if (cellClicked != null) {
                    if (cellClicked.Texture != 1 && cellClicked.Texture != 3) {
                        if (cellClicked.Texture == 0) {
                            cellClicked.Texture = 2;
                            Flags++;
                            SoundFlag.Play();
                        } else {
                            cellClicked.Texture = 0;
                            Flags--;
                            SoundRemoveFlag.Play();
                        }
                    }
                }
            }

            if (GameState.MouseState.LeftButton == ButtonState.Pressed && GameState.LastMouseState.LeftButton == ButtonState.Released) {
                Cell cellClicked = Grid.ClickOnCell();
                if (cellClicked != null) {
                    if (cellClicked.Texture != 2) {
                        if (cellClicked.Mine) {
                            cellClicked.Texture = 3;
                            SoundExplosion.Play();
                            Change(GameState.State.GameOver);
                        } else {
                            cellClicked.Texture = 1;
                            Grid.DiscoverNearbyCells(cellClicked.GridLine, cellClicked.GridColumn);
                        }
                    }
                }
            }

            Grid.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Grid.Draw(gameTime, spriteBatch);

            int x;
            int y;
            int margin = 5;
            string str;
            float lineHeight = Font.MeasureString("X").Y;

            str = "Mines restantes: " + (Mines - Flags).ToString() + " - Temps: " + Time.Hours + "h " + Time.Minutes + "m " + Time.Seconds + "s";
            x = Grid.OffsetX;
            y = margin;
            spriteBatch.DrawString(Font, str, new Vector2(x, y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0.9f);

            base.Draw(gameTime, spriteBatch);
        }
    }
}
