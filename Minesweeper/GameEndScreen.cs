﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Minesweeper
{
    public class GameEndScreen : MenuScreen
    {
        private static GameEndScreen instance;

        public static GameEndScreen Instance {
            get {
                if (instance == null) {
                    instance = new GameEndScreen();
                }
                return instance;
            }
        }
        public SpriteFont MainFont { get; private set; }

        public GameEndScreen() : base()
        {
            MenuItems = new List<MenuItem>() {
                new MenuItem("Nouvelle partie", GameState.State.GameModeMenu, true),
                new MenuItem("Retour au menu", GameState.State.MainMenu)
            };

            LoadContent();
        }

        public override void LoadContent()
        {
            MainFont = Content.Load<SpriteFont>("fonts/main");
            Song = Content.Load<Song>("musics/Yeah");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (GameState.KeyboardState.KeyPressedOnce(Keys.Escape)) {
                Change(GameState.State.Game);
                return;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            GameScreen.Instance.Draw(gameTime, spriteBatch);

            DrawOverlay(spriteBatch);

            int lineHeight = (int)MainFont.MeasureString("X").Y;
            int y = 100;
            string str = "BRAVO !";
            spriteBatch.DrawString(MainFont, str, new Vector2((Game1.Instance.GraphicsDevice.Viewport.Width / 2) - (MainFont.MeasureString(str).X / 2), y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);

            y += lineHeight + 10;
            str = "Temps: " + GameScreen.Instance.Time.Hours + "h " + GameScreen.Instance.Time.Minutes + "m " + GameScreen.Instance.Time.Seconds + "s";
            spriteBatch.DrawString(MainFont, str, new Vector2((Game1.Instance.GraphicsDevice.Viewport.Width / 2) - (MainFont.MeasureString(str).X / 2), y), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);

            base.Draw(gameTime, spriteBatch);
        }
    }
}
