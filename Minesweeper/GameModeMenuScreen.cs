﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Minesweeper
{
    public class GameModeMenuScreen : MenuScreen
    {
        private static GameModeMenuScreen instance;

        public static GameModeMenuScreen Instance {
            get {
                if (instance == null) {
                    instance = new GameModeMenuScreen();
                }
                return instance;
            }
        }

        public GameModeMenuScreen() : base()
        {
            MenuItems = new List<MenuItem>() {
                new MenuItem("Grille 9x9", GameState.State.Game, 9, 9, 10, true),
                new MenuItem("Grille 16x16", GameState.State.Game, 16, 16, 40),
                new MenuItem("Grille 30x16", GameState.State.Game, 30, 16, 99),
                new MenuItem("Retour", GameState.State.MainMenu)
            };

            LoadContent();
        }

        public override void LoadContent()
        {
            Song = Content.Load<Song>("musics/Differences");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (GameState.KeyboardState.KeyPressedOnce(Keys.Escape)) {
                Change(GameState.State.MainMenu);
                return;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
        }
    }
}
