﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Minesweeper
{
    public class MenuScreen : Screen
    {
        public List<MenuItem> MenuItems { get; protected set; }
        public SpriteFont Font { get; set; }

        public MenuScreen() : base()
        {
            MenuItems = new List<MenuItem>();

            LoadContent();
        }

        public override void LoadContent()
        {
            Font = Content.Load<SpriteFont>("fonts/menu");

            float lineHeight = Font.MeasureString("X").Y + 10;
            float posX;
            float posY = (Game1.Instance.GraphicsDevice.Viewport.Height / 2) - ((MenuItems.Count * lineHeight) / 2);
            foreach (MenuItem menuItem in MenuItems) {
                posX = (Game1.Instance.GraphicsDevice.Viewport.Width / 2) - (Font.MeasureString(menuItem.Title).X / 2);
                menuItem.Bounds = new Rectangle((int)posX, (int)posY, (int)Font.MeasureString(menuItem.Title).X, (int)Font.MeasureString(menuItem.Title).Y);
                posY += lineHeight;
            }

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            MenuItem activeMenu = null;
            foreach (MenuItem menuItem in MenuItems) {
                if ((GameState.MouseState.X >= menuItem.Bounds.X && GameState.MouseState.X <= menuItem.Bounds.X + menuItem.Bounds.Width) &&
                    (GameState.MouseState.Y >= menuItem.Bounds.Y && GameState.MouseState.Y <= menuItem.Bounds.Y + menuItem.Bounds.Height)) {
                    activeMenu = menuItem;

                    if (GameState.MouseState.LeftButton == ButtonState.Pressed && GameState.LastMouseState.LeftButton == ButtonState.Released) {
                        if (menuItem.ScreenTarget == GameState.State.Game && GameState.CurrentState == GameState.State.GameModeMenu) {
                            GameScreen.Instance = new GameScreen(menuItem.GameMines, menuItem.GameGridWidth, menuItem.GameGridHeight);
                        }
                        Change(menuItem.ScreenTarget);
                        return;
                    }
                }
            }

            if (activeMenu != null) {
                activeMenu.Active = true;
                foreach (MenuItem menuItem in MenuItems) {
                    if (!ReferenceEquals(activeMenu, menuItem)) {
                        menuItem.Active = false;
                    }
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (MenuItem menuItem in MenuItems) {
                Color color = Color.LightSlateGray;
                if (menuItem.Active) {
                    color = Color.White;
                }
                spriteBatch.DrawString(Font, menuItem.Title, new Vector2(menuItem.Bounds.X, menuItem.Bounds.Y), color, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            }

            base.Draw(gameTime, spriteBatch);
        }

        public void DrawOverlay(SpriteBatch spriteBatch)
        {
            Texture2D overlay = new Texture2D(Game1.Instance.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            overlay.SetData<Color>(new Color[] { new Color(Color.Black, 0.6f) });
            spriteBatch.Draw(overlay, new Rectangle(0, 0, Game1.Instance.GraphicsDevice.Viewport.Width, Game1.Instance.GraphicsDevice.Viewport.Height), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0.00001f);
        }
    }
}
