﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Minesweeper
{
    public class MainMenuScreen : MenuScreen
    {
        private static MainMenuScreen instance;

        public static MainMenuScreen Instance {
            get {
                if (instance == null) {
                    instance = new MainMenuScreen();
                }
                return instance;
            }
        }
        public SpriteFont MainFont { get; private set; }

        public MainMenuScreen() : base()
        {
            MenuItems = new List<MenuItem>() {
                new MenuItem("Nouvelle partie", GameState.State.GameModeMenu, true),
                new MenuItem("Revenir au bureau", GameState.State.Exit)
            };

            LoadContent();
        }

        public override void LoadContent()
        {
            MainFont = Content.Load<SpriteFont>("fonts/main");
            Song = Content.Load<Song>("musics/Differences");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (GameState.KeyboardState.KeyPressedOnce(Keys.Escape)) {
                Change(GameState.State.Exit);
                return;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            string str = "Seeow's Minesweeper";
            spriteBatch.DrawString(MainFont, str, new Vector2((Game1.Instance.GraphicsDevice.Viewport.Width / 2) - (MainFont.MeasureString(str).X / 2), 100), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);

            base.Draw(gameTime, spriteBatch);
        }
    }
}
