﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Minesweeper
{
    public class Grid
    {
        public int OffsetX { get; private set; }
        public int OffsetY { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public Cell[][] Cells { get; private set; }
        public Random Random { get; private set; }

        public Grid(int width, int height, int numberMines)
        {
            Random = new Random();

            Width = width;
            Height = height;

            int marginRight = 4;
            int marginBottom = 3;

            int newCellSize = (Game1.Instance.GraphicsDevice.Viewport.Height - 80) / Height;
            int cellSize = newCellSize;
            cellSize = Game1.Instance.GraphicsDevice.Viewport.Width / Width;
            if (newCellSize < cellSize) {
                cellSize = newCellSize;
            }

            OffsetX = (Game1.Instance.GraphicsDevice.Viewport.Width / 2) - ((cellSize * Width) / 2);
            OffsetY = (Game1.Instance.GraphicsDevice.Viewport.Height / 2) - ((cellSize * Height) / 2);

            List<Texture2D> textures = new List<Texture2D>() {
                Game1.Instance.Content.Load<Texture2D>("images/tile0"),
                Game1.Instance.Content.Load<Texture2D>("images/tile1"),
                Game1.Instance.Content.Load<Texture2D>("images/tile2"),
                Game1.Instance.Content.Load<Texture2D>("images/tile3")
            };

            int x = OffsetX;
            int y = OffsetY;

            Cells = new Cell[Height][];
            for (int line = 0; line < Height; line++) {
                Cells[line] = new Cell[Width];
                for (int column = 0; column < Width; column++) {
                    Cells[line][column] = new Cell(textures, new Vector2(x, y), cellSize, marginRight, marginBottom, line, column);

                    x += cellSize;
                }

                y += cellSize;
                x = OffsetX;
            }

            int cptMines = 0;
            while (cptMines < numberMines) {
                int line = Random.Next(Height);
                int column = Random.Next(Width);
                Cell cell = Cells[line][column];

                if (cell.Mine == false) {
                    cell.Mine = true;
                    cptMines++;
                }
            }

            for (int line = 0; line < Cells.Length; line++) {
                for (int column = 0; column < Cells[line].Length; column++) {
                    for (int checkLine = line - 1; checkLine <= line + 1; checkLine++) {
                        if (checkLine >= 0 && checkLine < Cells.Length) {
                            for (int checkColumn = column - 1; checkColumn <= column + 1; checkColumn++) {
                                if (checkColumn >= 0 && checkColumn < Cells[checkLine].Length) {
                                    if (Cells[checkLine][checkColumn].Mine) {
                                        Cells[line][column].NearbyMines += 1;
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (Cell[] line in Cells) {
                foreach (Cell cell in line) {
                    cell.Draw(gameTime, spriteBatch);
                }
            }
        }

        public Cell ClickOnCell()
        {
            foreach (Cell[] line in Cells) {
                foreach (Cell cell in line) {
                    if ((GameState.MouseState.X >= cell.Position.X && GameState.MouseState.X <= cell.Position.X + cell.Width) &&
                    (GameState.MouseState.Y >= cell.Position.Y && GameState.MouseState.Y <= cell.Position.Y + cell.Height)) {
                        return cell;
                    }
                }
            }

            return null;
        }

        public void DiscoverNearbyCells(int line, int column)
        {
            Cell cell = Cells[line][column];

            if (cell.Mine == false) {
                int checkLine = line;
                int checkColumn = column + 1;
                if (checkLine >= 0 && checkLine < Cells.Length && checkColumn >= 0 && checkColumn < Cells[checkLine].Length) {
                    if (Cells[checkLine][checkColumn].Mine == false && Cells[checkLine][checkColumn].Texture != 1) {
                        Cells[checkLine][checkColumn].Texture = 1;
                        if (Cells[checkLine][checkColumn].NearbyMines == 0) {
                            DiscoverNearbyCells(checkLine, checkColumn);
                        }
                    }
                }

                checkLine = line + 1;
                checkColumn = column;
                if (checkLine >= 0 && checkLine < Cells.Length && checkColumn >= 0 && checkColumn < Cells[checkLine].Length) {
                    if (Cells[checkLine][checkColumn].Mine == false && Cells[checkLine][checkColumn].Texture != 1) {
                        Cells[checkLine][checkColumn].Texture = 1;
                        if (Cells[checkLine][checkColumn].NearbyMines == 0) {
                            DiscoverNearbyCells(checkLine, checkColumn);
                        }
                    }
                }

                checkLine = line;
                checkColumn = column - 1;
                if (checkLine >= 0 && checkLine < Cells.Length && checkColumn >= 0 && checkColumn < Cells[checkLine].Length) {
                    if (Cells[checkLine][checkColumn].Mine == false && Cells[checkLine][checkColumn].Texture != 1) {
                        Cells[checkLine][checkColumn].Texture = 1;
                        if (Cells[checkLine][checkColumn].NearbyMines == 0) {
                            DiscoverNearbyCells(checkLine, checkColumn);
                        }
                    }
                }

                checkLine = line - 1;
                checkColumn = column;
                if (checkLine >= 0 && checkLine < Cells.Length && checkColumn >= 0 && checkColumn < Cells[checkLine].Length) {
                    if (Cells[checkLine][checkColumn].Mine == false && Cells[checkLine][checkColumn].Texture != 1) {
                        Cells[checkLine][checkColumn].Texture = 1;
                        if (Cells[checkLine][checkColumn].NearbyMines == 0) {
                            DiscoverNearbyCells(checkLine, checkColumn);
                        }
                    }
                }
            }
        }
    }
}
