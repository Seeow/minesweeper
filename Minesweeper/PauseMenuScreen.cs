﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Minesweeper
{
    public class PauseMenuScreen : MenuScreen
    {
        private static PauseMenuScreen instance;

        public static PauseMenuScreen Instance {
            get {
                if (instance == null) {
                    instance = new PauseMenuScreen();
                }
                return instance;
            }
        }

        public PauseMenuScreen() : base()
        {
            MenuItems = new List<MenuItem>() {
                new MenuItem("Reprendre", GameState.State.Game, true),
                new MenuItem("Quitter la partie", GameState.State.MainMenu)
            };

            LoadContent();
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (GameState.KeyboardState.KeyPressedOnce(Keys.Escape)) {
                Change(GameState.State.Game);
                return;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            GameScreen.Instance.Draw(gameTime, spriteBatch);

            DrawOverlay(spriteBatch);

            base.Draw(gameTime, spriteBatch);
        }
    }
}
