﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Minesweeper
{
    public static class GameState
    {
        public static KeyboardState KeyboardState { get; set; }
        public static KeyboardState LastKeyboardState { get; set; }
        public static MouseState MouseState { get; set; }
        public static MouseState LastMouseState { get; set; }
        public static Screen Screen { get; set; }
        public static Song CurrentSong { get; set; }
        public static State CurrentState { get; set; }
        public static State LastState { get; set; }
        public enum State
        {
            Game,
            GameModeMenu,
            MainMenu,
            PauseMenu,
            GameOver,
            GameEnd,
            Exit
        }
    }
}
