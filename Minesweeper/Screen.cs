﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace Minesweeper
{
    public class Screen
    {
        public ContentManager Content { get; private set; }
        public List<Sprite> Sprites { get; protected set; }
        public Song Song { get; protected set; }

        public Screen()
        {
            Content = Game1.Instance.Content;
            Sprites = new List<Sprite>();
        }

        public virtual void LoadContent()
        {

        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }

        public static void Change(GameState.State newState)
        {
            GameState.LastState = GameState.CurrentState;
            GameState.CurrentState = newState;

            switch (GameState.CurrentState) {
                case GameState.State.Game:
                    GameState.Screen = GameScreen.Instance;
                    break;
                case GameState.State.PauseMenu:
                    GameState.Screen = PauseMenuScreen.Instance;
                    break;
                case GameState.State.Exit:
                    Game1.Instance.Exit();
                    break;
                case GameState.State.GameModeMenu:
                    GameScreen.Instance = null;
                    GameState.Screen = GameModeMenuScreen.Instance;
                    break;
                case GameState.State.GameOver:
                    GameState.Screen = GameOverScreen.Instance;
                    break;
                case GameState.State.GameEnd:
                    GameState.Screen = GameEndScreen.Instance;
                    break;
                case GameState.State.MainMenu:
                default:
                    GameState.Screen = MainMenuScreen.Instance;
                    break;
            }

            GameState.Screen.PlaySong(GameState.Screen.Song);
        }

        public void AddSprite(Sprite newSprite)
        {
            Sprites.Add(newSprite);
        }

        public void PlaySong(Song song)
        {
            if (song != null && GameState.CurrentSong != song) {
                MediaPlayer.Play(song);
                GameState.CurrentSong = song;
            }
        }
    }
}
