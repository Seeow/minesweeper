﻿using Microsoft.Xna.Framework.Input;

namespace Minesweeper
{
    public static class Input
    {
        public static bool KeyPressedOnce(this KeyboardState keyboardState, Keys key)
        {
            if (keyboardState.IsKeyDown(key) && !GameState.LastKeyboardState.IsKeyDown(key)) {
                return true;
            }

            return false;
        }
    }
}
